from django.conf.urls import patterns, include, url
from meHungry_customer_website import views
from meHungry_customer_website.views import *
from django.contrib.auth.views import login, logout
from ajax_select import urls as ajax_select_urls
from django.conf import settings
from django.contrib import admin
from rest_framework.routers import DefaultRouter

admin.autodiscover()

#DRF routing
router = DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'addresses', views.AddressViewSet)
router.register(r'restaurants', views.RestaurantViewSet)
router.register(r'food', views.FoodItemViewSet)
router.register(r'menus', views.MenuViewSet)
router.register(r'weekly_menus', views.WeeklyMenuViewSet)

urlpatterns = patterns('',
    url(r'^api/', include(router.urls)),
    url(r'^$', homepage_view, name='homepage_view'),
    url(r'^about/', about, name='about'),
    url('', include('django.contrib.auth.urls', namespace='auth')),
    url(r'^grappelli/', include('grappelli.urls')),  # grappelli URLS
    # include the lookup urls
    url(r'^admin/lookups/', include(ajax_select_urls)),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/login/', login, name='login'),
    url(r'^accounts/logout', logout, {'next_page': '/'}),
    url(r'^accounts/signup/$', SignupWizard.as_view([UserForm, PhoneForm, PhoneVerifyForm]), name='sign_up'),
    # take out phone verification for now. need to revisit
    # url(r'^accounts/signup/$', SignupWizard.as_view([UserForm, PhoneForm]), name='sign_up'),
    url(r'^accounts/profile/', user_home),
    #Password reset
    url(r'^resetpassword/passwordsent/$', 'django.contrib.auth.views.password_reset_done', name='password_reset_done'),
    url(r'^resetpassword/$', 'django.contrib.auth.views.password_reset', {'password_reset_form': EmailValidationForgotPW}, name='reset_password'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', 'django.contrib.auth.views.password_reset_confirm',name='password_reset_confirm'),
    url(r'^reset/done/$', 'django.contrib.auth.views.password_reset_complete', name= 'password_reset_complete'),
    url(r'^week/(?P<address>\d+)', display_menus, name='display_menus'),
    url(r'^my_orders/', my_orders, name="order_hist"),
    url(r'^food/(?P<address>\d+)/(?P<daily_menu>\d+)', food, name="view_food"),
    url(r'^food_order/add/$', add_to_cart, name='item_added'),
    url(r'^food_order/remove/$', remove_from_cart, name='item_removed'),
    # url(r'^get_stripe_user/',charge_customer),
    url(r'^charge_customer/(?P<menu_id>\d+)', charge_customer),
    # Stripe Payment URLS
    url(r'^payments/', include("payments.urls")),
    # Social auth for google login
    url('', include('social.apps.django_app.urls', namespace='social')),
    #url(r'/google_login/$',RedirectView.as_view(pattern_name='socialauth_begin')),
    #url(r'^/complete/google-oauth2/', google_logi)
    # url(r'^phone_verify',phone_verify),

    #DRF
    # url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^rest-auth/', include('rest_auth.urls')),
)
urlpatterns += patterns('', (r'^static/(.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}), )