from django.http import HttpRequest
from models import CustomerProfile
from django.shortcuts import get_object_or_404


def user_address(request):
    if 'admin' in request.META['PATH_INFO']:
        return {}
    if request.user.is_authenticated():
        address = get_object_or_404(CustomerProfile, user=request.user.id).address.id
    else:
        address = 1
    return {
        'address': address
    }

