from django.conf import settings
from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from django.contrib.sites.models import Site
from payments.models import Charge, Customer
from django.core.mail import send_mail
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.contrib.contenttypes.models import ContentType
from phonenumber_field.modelfields import PhoneNumberField
from datetime import datetime
from jsonfield import JSONField

DAYS_OF_WEEK = (
    (0, 'Monday'),
    (1, 'Tuesday'),
    (2, 'Wednesday'),
    (3, 'Thursday'),
    (4, 'Friday')
)

# Create your models here.

class MyUserManager(BaseUserManager):
    def create_user(self, email, password, is_admin, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        now = timezone.now()
        if not email:
            raise ValueError('Users must have an email address')
        email = self.normalize_email(email)
        user = self.model(email=email,
                          is_staff=is_admin, is_active=True,
                          is_superuser=is_superuser, last_login=now,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **extra_fields):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        now = timezone.now()
        if not email:
            raise ValueError('Users must have an email address')
        email = self.normalize_email(email)
        user = self.model(email=email,
                          is_staff=True, is_active=True,
                          is_superuser=True, last_login=now,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user


class MyAbstractUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
        blank=False,
        null=False
    )
    first_name = models.CharField(_('first name'), max_length=30, blank=False, null=False)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)

    is_staff = models.BooleanField(_('admin status'), default=False,
                                   help_text=_('Designates whether the user can log into this admin '
                                               'site.'))

    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField('date joined', default=timezone.now)

    objects = MyUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        abstract = True

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)


class MyUser(MyAbstractUser):

    """
    Concrete class of AbstractEmailUser.
    Use this if you don't need to extend EmailUser.
    """

    class Meta(MyAbstractUser.Meta):
        swappable = 'AUTH_USER_MODEL'


class CustomerProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, limit_choices_to={'groups': "2"}, null=True, related_name='user')
    payment_info = models.OneToOneField(Customer, null=True)
    address = models.ForeignKey('Address', null=True)
    phone = PhoneNumberField(null=True)
    phone_notifications = models.BooleanField(null=False, default=True)
    email_notifications = models.BooleanField(null=False, default=True)

    def __unicode__(self):
        return self.user.email


class Address(models.Model):
    desc = models.TextField(null=True)
    street = models.CharField(max_length=50, null=True)
    apt = models.CharField(max_length=50, null=True, blank=True)
    city = models.CharField(max_length=20, null=True)
    state = models.CharField(max_length=20, null=True)
    zip = models.IntegerField(max_length=5, null=True)

    def __unicode__(self):
        return self.desc


# menu representation (contains restaurant info and food items, based on certain day)
class Menu(models.Model):
    Restaurant = models.ForeignKey('Restaurant', null=False)
    foodItems = models.ManyToManyField('FoodItem', null=False)
    day = models.PositiveIntegerField(choices=DAYS_OF_WEEK)

    def __unicode__(self):
        return self.Restaurant.name + " " + str(self.get_day_display()) + " menu"

    class Meta:
        ordering = ('day',)


class FoodItem(models.Model):
    Restaurant = models.ForeignKey('Restaurant', null=False)
    itemName = models.CharField(max_length=50, null=True)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    vegetarian = models.BooleanField(default=False)
    desc = models.TextField(max_length=400, null=True)
    imageurl = models.TextField(blank=True)

    def __unicode__(self):
        return self.itemName

    def priceformat(self):
        return "$%0.2f" % self.price


class Restaurant(models.Model):
    name = models.CharField(max_length=50)
    cuisine = models.CharField(max_length=15)
    imageurl = models.TextField(blank=True)
    websiteurl = models.TextField(blank=True)
    desc = models.TextField(max_length=500)
    addressLine1 = models.CharField(max_length=50)
    addressLine2 = models.CharField(max_length=50, blank=True)
    city = models.CharField(max_length=50)
    state = models.CharField(max_length=10)
    # tags to be implemented for the search engine
    # tags  =
    delivery = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name


# weekly menu for a certain address
class WeeklyMenu(models.Model):
    dailyMenus = models.ManyToManyField(Menu)
    address = models.ForeignKey(Address)
    ordering = models.BooleanField(default=True)

    def __unicode__(self):
        return self.address.desc + " Weekly Menu"


class FoodOrder(models.Model):
    created = models.DateTimeField(editable=False)
    modified = models.DateTimeField()

    # do we need restaurant?
    restaurant = models.ForeignKey(Restaurant, null=True)
    # "groups" is the group.id from the auth_group table
    customer = models.ForeignKey(settings.AUTH_USER_MODEL, limit_choices_to={'groups': "2"}, null=True)
    orderItems = models.ManyToManyField(FoodItem, null=True)
    # vendorInfo = models.ForeignKey(User, related_name='is_staff'
    # Price of the order according to the Menu
    #menuPrice = models.DecimalField(max_digits=6, decimal_places=2)

    charge_info = models.OneToOneField(Charge,null=True)
    order_price = models.DecimalField(max_digits=6,decimal_places=2,null=True)

    delivered = models.BooleanField(null=False, default=False)

    # Price offered by the customer for the food order
    # bidPrice = models.DecimalField(max_digits=6, decimal_places=2)
    # foodItems attribute will contain the customer's order in xml formant
    # foodItems =

    address_info = models.ForeignKey(Address, null=True)

    delivery = models.BooleanField(default=False)


    # Logic regarding order status to be added
    #order_status =



    # paymentMethod to be activated while implementing the transaction engine
    # paymentMethod  =
    # To check the status of the order option set to be implemented
    # orderStatus
    # vendorApproval to be determined after the order is viewed by the vendor
    # vendorApproval =  option set to be implemented

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created = datetime.today()
        self.modified = datetime.today()
        return super(FoodOrder, self).save(*args, **kwargs)


    def __unicode__(self):
        return self.customer.email


class Order(models.Model):
    created = models.DateTimeField(editable=False)
    modified = models.DateTimeField()

    customer = models.ForeignKey(settings.AUTH_USER_MODEL, limit_choices_to={'groups': "2"}, null=True, editable=False)
    # do we need restaurant?
    restaurant = models.ForeignKey(Restaurant, null=True, editable=False)
    # "groups" is the group.id from the auth_group table
    customer_phone = PhoneNumberField(default="+10000000000", editable=False)
    order_items = JSONField(null=True, editable=False)
    # vendorInfo = models.ForeignKey(User, related_name='is_staff'
    # Price of the order according to the Menu
    #menuPrice = models.DecimalField(max_digits=6, decimal_places=2)

    charge_info = models.CharField(null=True, max_length=100, editable=False)
    order_price = models.DecimalField(max_digits=6, decimal_places=2, null=True, editable=False)

    delivered = models.BooleanField(null=False, default=False)
    order_for = models.CharField(null=True, max_length=20)
    # Price offered by the customer for the food order
    # bidPrice = models.DecimalField(max_digits=6, decimal_places=2)
    # foodItems attribute will contain the customer's order in xml formant
    # foodItems =

    address_info = models.ForeignKey(Address, null=True,  editable=False)

    delivery = models.BooleanField(default=False)


    # Logic regarding order status to be added
    #order_status =



    # paymentMethod to be activated while implementing the transaction engine
    # paymentMethod  =
    # To check the status of the order option set to be implemented
    # orderStatus
    # vendorApproval to be determined after the order is viewed by the vendor
    # vendorApproval =  option set to be implemented

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created = datetime.today()
        self.modified = datetime.today()
        return super(Order, self).save(*args, **kwargs)


    def __unicode__(self):
        return self.customer.email


class Deliveries(models.Model):
    deliveryPerson = models.OneToOneField(settings.AUTH_USER_MODEL, limit_choices_to={'groups': "3"}, null=True)
    foodOrderInfo = models.ForeignKey(FoodOrder)
    deliveryCompleted = models.BooleanField(default=False)
    '''deliveryApt = models.ForeignKey(FoodOrder.deliveryApt)
    deliveryCity = models.ForeignKey(FoodOrder.deliveryCity)
    deliveryState = models.ForeignKey(FoodOrder.deliveryState)
    deliveryZip = models.ForeignKey(FoodOrder.deliveryZip)'''

    def __unicode__(self):
        return self.deliveryPerson.username


class PickUps(models.Model):
    pickUpPerson = models.ForeignKey(settings.AUTH_USER_MODEL, limit_choices_to={'groups': "3"}, null=True)
    pickUpRestaurant = models.ForeignKey(FoodOrder, null=True)



# class Referals(models.Model):
#
#     refering_customer = models.ForeignKey(settings.AUTH_USER_MODEL, limit_choices_to={'groups':"2"}, null=True,related_name='referencingCustomer')
#
#     referance_code = models.IntegerField(max_length=6,unique=True) # Randomly generated referance code
#
#     referals_left = 3 # Max number of discounts available to each user.
#
#     referals_earned=0 # Number of discounts remaining.
#
#     referal_cashed =0 # Total discounts used to date.
#
#     referenced_users= models.ManyToManyField(settings.AUTH_USER_MODEL,limit_choices_to={'groups':"2"}, null=True)
#
#
#     def _unicode_(self):
#         return self.customer.username
