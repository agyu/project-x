from rest_framework import serializers
from models import MyUser, CustomerProfile, Address, \
    Restaurant, FoodItem, WeeklyMenu, Menu
from phonenumber_field import phonenumber


class PhoneField(serializers.Field):

    def to_representation(self, obj):
        return obj.__unicode__()

    def to_internal_value(self, data):
        return phonenumber.to_python(data)


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = MyUser
        fields = ('email', 'first_name', 'last_name',)
        read_only_fields = ('last_login',)


class UserUpdateSerializer(UserSerializer):
    # address = serializers.ChoiceField(Address.objects.all())
    phone = PhoneField(source="user.phone")
    phone_notifications = serializers.BooleanField(source="user.phone_notifications")
    email_notifications = serializers.BooleanField(source="user.email_notifications")

    class Meta(UserSerializer.Meta):
        fields = UserSerializer.Meta.fields + ('phone', 'phone_notifications', 'email_notifications',)

    def update(self, instance, validated_data):
        profile_data = validated_data.pop('user', {})
        phone_notifications = profile_data.get('phone_notifications')
        email_notifications = profile_data.get('email_notifications')
        phone = profile_data.get('phone')

        instance = super(UserSerializer, self).update(instance, validated_data)

        customer_profile = instance.user
        if profile_data:
            if phone_notifications:
                customer_profile.phone_notifications = phone_notifications
            if email_notifications:
                customer_profile.email_notifications = email_notifications
            if phone:
                phone_number = phonenumber.to_python(phone)
                if not phone_number.is_valid():
                    raise serializers.ValidationError('Invalid phone number')
                customer_profile.phone = phone
            customer_profile.save()
        return instance


class AddressSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Address
        fields = ('url', 'id', 'desc', 'street', 'apt', 'city', 'state', 'zip')


class RestaurantSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Restaurant
        fields = ('url', 'name', 'cuisine', 'imageurl',
                  'websiteurl', 'desc', 'addressLine1',
                  'addressLine2', 'city', 'state')


class FoodItemSerializer(serializers.ModelSerializer):
    Restaurant = RestaurantSerializer()

    class Meta:
        model = FoodItem
        fields = ('id', 'Restaurant', 'itemName', 'price', 'vegetarian', 'desc', 'imageurl')


class MenuSerializer(serializers.HyperlinkedModelSerializer):
    Restaurant = serializers.HyperlinkedRelatedField(
        view_name='restaurant-detail',
        read_only=True
    )

    foodItems = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='fooditem-detail'
    )

    class Meta:
        model = Menu
        fields = ('url', 'Restaurant', 'foodItems', 'day')


class WeeklyMenuSerializer(serializers.HyperlinkedModelSerializer):
    address = serializers.StringRelatedField()

    dailyMenus = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='menu-detail'
    )

    class Meta:
        model = WeeklyMenu
        fields = ('url', 'address', 'dailyMenus')