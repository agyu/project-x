from meHungry_customer_website.models import *
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _
from forms import MyUserChangeForm, MyUserCreationForm
from django.contrib.sessions.models import Session
from import_export.admin import ImportExportModelAdmin
from import_export import resources


class SessionAdmin(admin.ModelAdmin):
    def _session_data(self, obj):
        return obj.get_decoded()
    list_display = ['session_key', '_session_data', 'expire_date']
    list_filter = ['expire_date']
    search_fields = ['expire_date']
admin.site.register(Session, SessionAdmin)


# Register your models here.
class MyUserAdmin(UserAdmin):

    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1'),
        }),
    )

    # The forms to add and change user instances
    form = MyUserChangeForm
    add_form = MyUserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('email', 'first_name', 'last_name', 'is_staff')
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups')
    search_fields = ('email', 'first_name', 'last_name')
    ordering = ('email',)
    filter_horizontal = ('groups', 'user_permissions',)


class CustomerProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'payment_info', 'address', 'phone', 'phone_notifications', 'email_notifications')
    list_filter = ['user', 'payment_info', 'address', 'phone', 'phone_notifications', 'email_notifications']
    search_fields = ['user', 'phone', 'phone_notifications', 'email_notifications']


class MenuAdmin(admin.ModelAdmin):
    list_display = ('Restaurant', 'day')
    list_filter = ['Restaurant', 'foodItems', 'day']
    search_fields = ['Restaurant', 'foodItems', 'day']


class FoodItemAdmin(admin.ModelAdmin):
    list_display = ('id', 'Restaurant', 'itemName', 'price', 'vegetarian', 'desc')
    list_filter = ['id', 'Restaurant', 'itemName', 'price', 'vegetarian', 'desc']
    search_fields = ['id', 'itemName']


class WeeklyMenuAdmin(admin.ModelAdmin):
    list_filter = ['dailyMenus', 'address', 'ordering']
    search_fields = ['dailyMenus', 'address', 'ordering']


class AddressAdmin(admin.ModelAdmin):
    list_display = ('desc', 'street')
    list_filter = ['desc', 'street', 'apt', 'city', 'state', 'zip']
    search_fields = ['desc', 'street', 'city', 'zip', 'state']


class OrderResource(resources.ModelResource):
    class Meta:
        model = Order
        fields = ('customer__first_name', 'customer__last_name', 'order_for', 'order_items',
                  'customer_phone', 'order_price', 'address_info__desc', 'restaurant__name', )
        export_order = ('order_for','customer_phone', 'customer__first_name', 'customer__last_name',  'order_items',
                        'order_price', 'address_info__desc', 'restaurant__name', )

class OrderAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    resource_class = OrderResource
    list_display = ('name', 'customer', 'delivered', 'order_for', 'restaurant', 'order_items',
                    'customer_phone', 'charge_info', 'order_price', 'address_info', 'created')
    list_filter = ['customer','delivered', 'order_for', 'restaurant', 'order_items',
                   'customer_phone', 'charge_info',   'order_price', 'address_info', 'created']
    search_fields = ['customer','delivered', 'order_for', 'restaurant',
                     'order_items', 'customer_phone', 'charge_info',   'order_price', 'address_info', 'created']

    actions = ['make_delivered']

    def name(self, obj):
        return obj.customer.first_name + " " + obj.customer.last_name
    name.short_description = 'Name'

    def make_delivered(self, request, queryset):
        rows_updated = queryset.update(delivered=True)

        if rows_updated == 1:
                message_bit = "1 order was"
        else:
            message_bit = "%s orders were" % rows_updated
        self.message_user(request, "%s successfully marked as delivered." % message_bit)
    make_delivered.short_description = "Mark selected orders as delivered."

# Register the new EmailUserAdmin
admin.site.register(MyUser, MyUserAdmin)
admin.site.register(CustomerProfile, CustomerProfileAdmin)
admin.site.register(Address, AddressAdmin)
admin.site.register(Menu, MenuAdmin)
admin.site.register(Restaurant)
admin.site.register(FoodItem, FoodItemAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(Deliveries)
admin.site.register(PickUps)
admin.site.register(WeeklyMenu, WeeklyMenuAdmin)


