# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('meHungry_customer_website', '0009_auto_20150513_1751'),
    ]

    operations = [
        migrations.AddField(
            model_name='weeklymenu',
            name='ordering',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
