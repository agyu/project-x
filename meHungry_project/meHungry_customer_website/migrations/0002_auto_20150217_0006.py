# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('meHungry_customer_website', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='menu',
            name='Restaurant',
            field=models.ForeignKey(to='meHungry_customer_website.Restaurant'),
            preserve_default=True,
        ),
    ]
