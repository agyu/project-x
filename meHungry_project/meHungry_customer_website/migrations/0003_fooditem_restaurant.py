# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('meHungry_customer_website', '0002_auto_20150217_0006'),
    ]

    operations = [
        migrations.AddField(
            model_name='fooditem',
            name='Restaurant',
            field=models.ForeignKey(default=1, to='meHungry_customer_website.Restaurant'),
            preserve_default=False,
        ),
    ]
