# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('meHungry_customer_website', '0006_auto_20150324_1826'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='menu',
            options={'ordering': ('day',)},
        ),
        migrations.AddField(
            model_name='customerprofile',
            name='email_notifications',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='customerprofile',
            name='phone_notifications',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='foodorder',
            name='delivered',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
