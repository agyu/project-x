# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields
import phonenumber_field.modelfields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('meHungry_customer_website', '0007_auto_20150420_1942'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('customer_phone', phonenumber_field.modelfields.PhoneNumberField(default=b'+10000000000', max_length=128, editable=False)),
                ('order_items', jsonfield.fields.JSONField(null=True, editable=False)),
                ('charge_info', models.CharField(max_length=100, null=True, editable=False)),
                ('order_price', models.DecimalField(null=True, editable=False, max_digits=6, decimal_places=2)),
                ('delivered', models.BooleanField(default=False)),
                ('delivery', models.BooleanField(default=False)),
                ('address_info', models.ForeignKey(editable=False, to='meHungry_customer_website.Address', null=True)),
                ('customer', models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True)),
                ('restaurant', models.ForeignKey(editable=False, to='meHungry_customer_website.Restaurant', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='fooditem',
            name='desc',
            field=models.TextField(max_length=400, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='fooditem',
            name='itemName',
            field=models.CharField(max_length=50, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='restaurant',
            name='desc',
            field=models.TextField(max_length=500),
            preserve_default=True,
        ),
    ]
