# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        ('meHungry_customer_website', '0004_fooditem_imageurl'),
    ]

    operations = [
        migrations.AddField(
            model_name='customerprofile',
            name='phone',
            field=phonenumber_field.modelfields.PhoneNumberField(max_length=128, null=True),
            preserve_default=True,
        ),
    ]
