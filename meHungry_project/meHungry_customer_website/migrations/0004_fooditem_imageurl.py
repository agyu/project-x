# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('meHungry_customer_website', '0003_fooditem_restaurant'),
    ]

    operations = [
        migrations.AddField(
            model_name='fooditem',
            name='imageurl',
            field=models.TextField(blank=True),
            preserve_default=True,
        ),
    ]
