# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('meHungry_customer_website', '0005_customerprofile_phone'),
    ]

    operations = [
        migrations.AddField(
            model_name='foodorder',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 24, 18, 26, 47, 145000), editable=False),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='foodorder',
            name='modified',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 24, 18, 26, 58, 674000)),
            preserve_default=False,
        ),
    ]
