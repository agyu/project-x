from django.http import HttpResponse
from django.template import RequestContext
from django.shortcuts import get_object_or_404, render_to_response, redirect
from forms import *
from meHungry_customer_website.models import *
from payments.models import *
from django.core.context_processors import csrf
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import Group
from carton.cart import *
from meHungry_customer_website.meHungry_Packages.customTwilio.phone_verify import *
from django.contrib.formtools.wizard.views import SessionWizardView
from datetime import datetime, timedelta

from rest_framework import viewsets, permissions, filters, generics
from rest_framework.decorators import list_route
from rest_framework.response import Response
from serializers import *
import os

cwd = os.getcwd()

# Create your views here.


def homepage_view(request):
    context = {}
    context.update({
        'title': "meHungry: Driven by Hunger",
        })
    return render_to_response('home.html',
                              context,
                              context_instance=RequestContext(request))


def about(request):
    return render_to_response('about.html', context_instance=RequestContext(request))


def google_login(request):
    redirect(user_home)


@login_required
def user_home(request):
    context = {}
    context.update(csrf(request))
    title = "meHungry Profile"

    success = None
    tempuser = request.user
    tempprofile = CustomerProfile.objects.get(user=tempuser)

    form = UpdateUserForm(initial={'email': tempuser.email,
                                   'first_name': tempuser.first_name,
                                   'last_name': tempuser.last_name
    }, prefix='user')

    form2 = AddressForm(initial={'address': tempprofile.address.pk}, prefix='address')

    if request.method == 'POST':
        success = False
        form = UpdateUserForm(request.POST, instance=request.user, prefix='user')
        if form.is_valid() and form.has_changed():
            user = form.save(commit=False)
            user.save()
            success = True
        form2 = AddressForm(request.POST, instance=tempprofile, prefix='address')
        if form2.is_valid() and form2.has_changed():
            profile = form2.save(commit=False,)
            profile.save()
            success = True
    phone_not = tempprofile.phone_notifications
    email_not = tempprofile.email_notifications
    context.update({'title': title,
                    'form': form,
                    'form2': form2,
                    'phone_not': phone_not,
                    'email_not': email_not,
                    'user': tempuser,
                    'success': success})
    return render_to_response('meHungry_customer_website/user_home.html', context, context_instance=RequestContext(request))


def display_menus(request, address):
    context = {}
    title = "meHungry Weekly Menu"
    #TODO: fix display for monday
    # Temporarily hardcoded the name of the chefs, as we will be using only one service provider for the beta test.
    # chef = get_object_or_404(Restaurant, address=address)
    menu_address = get_object_or_404(Address, pk=address)
    menu_list = get_object_or_404(WeeklyMenu, address=menu_address)
    saved_address = menu_address
    address_list = Address.objects.all()
    day = datetime.today().weekday()
    hour = datetime.now().hour

    if day == 5 or day == 6:
        day = 0

    if request.user.is_authenticated():
        saved_address = get_object_or_404(CustomerProfile, user=request.user.id).address
        if address != unicode(saved_address.id):
            return redirect('display_menus', address=unicode(saved_address.id))

    context.update({'title': title,
                    'ordering': menu_list.ordering,
                    'menu': menu_list.dailyMenus.all(),
                    'menu_id': menu_address.id,
                    'saved_address': saved_address.desc,
                    'day': day,
                    'hour': hour,
                    'addresses': address_list,
                    'user': request.user})

    return render_to_response("meHungry_customer_website/week.html", context, context_instance=RequestContext(request))


def check_can_order(day, menu_day):
    h = datetime.now().hour
    if day < menu_day:
        can_order = True
    elif day == menu_day and h < 11:
        can_order = True
    # if weekend
    elif day == 5 or day == 6:
        can_order = True
    else:
        can_order = False
    return can_order


def display_date(day, menu_day):
    # if saturday offset date by +2
    if day == 5:
        full_date = datetime.today() + timedelta(days=menu_day+2)
    # if sunday offset date by +1
    elif day == 6:
        full_date = datetime.today() + timedelta(days=menu_day+1)
    elif day < menu_day:
        delta = menu_day - day
        full_date = datetime.today() + timedelta(days=delta)
    else:
        delta = day - menu_day
        full_date = datetime.today() - timedelta(days=delta)
    return full_date


def food(request, address, daily_menu):
    if request.user.is_authenticated():
        saved_address = get_object_or_404(CustomerProfile, user=request.user.id).address
        if address != unicode(saved_address.id):
            return redirect('display_menus', address=unicode(saved_address.id))

    context = {}
    title = "meHungry Daily Menu"
    try:
        has_saved_card = Customer.objects.get(user=request.user.id).card_last_4
    except Customer.DoesNotExist:
        has_saved_card = None

    menu = get_object_or_404(Menu, pk=daily_menu)
    food_list = menu.foodItems.all()
    day = datetime.today().weekday()
    food_cart = Cart(session=request.session, session_key=('CART-'+str(daily_menu)))
    dollar_total = int(food_cart.total * Decimal(100))
    # can_order logic
    canorder = check_can_order(day, menu.day)

    # display date logic
    full_date = display_date(day, menu.day)
    yesterday = display_date(day, menu.day-1)
    finished_order = True
    if day < menu.day:
        finished_order = False

    context.update({'title': title,
                    'menu_id': menu.id,
                    'canorder': canorder,
                    'food_list': food_list,
                    'full_date': full_date,
                    'cart': food_cart,
                    'dollar_total': dollar_total,
                    'finished_order': finished_order,
                    'yesterday': yesterday,
                    'has_saved_card': has_saved_card,
                    'stripe_pk': settings.STRIPE_PUBLIC_KEY,
                    'is_logged_in': request.user.is_authenticated()})
    return render_to_response("meHungry_customer_website/menu.html", context, context_instance=RequestContext(request))


@login_required
def add_to_cart(request):
    context = {}
    context.update(csrf(request))
    if request.user.is_authenticated() and request.method == 'POST':
        food_item_id = request.POST.get('food_id', None)
        menu_id = request.POST.get('menu_id', None)
        food_item = get_object_or_404(FoodItem, pk=food_item_id)
        food_cart = Cart(session=request.session, session_key=('CART-'+str(menu_id)))
        food_cart.add(food_item, price=food_item.price)
        dollar_total = int(food_cart.total * Decimal(100))
        # save cart into session
        request.session['CART-'+str(menu_id)] = food_cart.cart_serializable
        # return cart data
        context = {'cart': food_cart,
                   'stripe_pk': settings.STRIPE_PUBLIC_KEY,
                   'dollar_total': dollar_total,
                   'menu_id': menu_id
        }
        resp = render_to_string('meHungry_customer_website/cart_template.html', context, context_instance=RequestContext(request))
        return HttpResponse(resp)
    else:
        return redirect('/accounts/login/')


@login_required
def remove_from_cart(request):
    context = {}
    context.update(csrf(request))
    if request.user.is_authenticated() and request.method == 'POST':
        food_item_id = request.POST.get('food_id', None)
        menu_id = request.POST.get('menu_id', None)
        food_item = get_object_or_404(FoodItem , pk=food_item_id)
        food_cart = Cart(session=request.session, session_key=('CART-'+str(menu_id)))
        food_cart.remove_single(food_item)
        dollar_total = int(food_cart.total * Decimal(100))
        # save cart into session
        request.session['CART-'+str(menu_id)] = food_cart.cart_serializable
        # return cart data
        context = {'cart': food_cart,
                   'stripe_pk': settings.STRIPE_PUBLIC_KEY,
                   'dollar_total': dollar_total,
                   'menu_id': menu_id
        }
        resp = render_to_string('meHungry_customer_website/cart_template.html', context, context_instance=RequestContext(request))
        return HttpResponse(resp)
    else:
        return redirect('/accounts/login/')


# Charge customer method for django-stripe-payments method
@login_required
def charge_customer(request, menu_id):

    context = {}
    context.update({'user': request.user})
    title="Checkout"
    menu = get_object_or_404(Menu, pk=menu_id)
    order_day = menu.get_day_display()
    day = datetime.today().weekday()
    can_order = check_can_order(day, menu.day)
    # Set your secret key: remember to change this to your live secret key in production
    # See your keys here https://dashboard.stripe.com/account

    if can_order and request.method == 'POST':
        stripe.api_key = settings.STRIPE_SECRET_KEY

        # Getting total price from the cart in the session
        food_cart = Cart(session=request.session, session_key=('CART-'+str(menu_id)))
        json_cart = {}
        for item in food_cart.items:
            json_cart[item.product.itemName] = item.quantity
        json_dump_cart = json.dumps(json_cart, sort_keys=True,
                                    indent=4, separators=(',', ': '))
        context.update({'title': title})
        total_amount = food_cart.total
        if total_amount <= 0.0:
            return render_to_response("meHungry_customer_website/empty_cart.html", context)

        customerProf = CustomerProfile.objects.get(user=request.user)
        stripe_customer = Customer.objects.get(user=request.user)
        rest = Menu.objects.get(pk=menu_id).Restaurant

        # Checking if user exists in Payments Database Customer Table
        try:

            # Important Note: stripe_user gets object from Customer table from OUR Payments Database
            # stripe_customer is an attribute of stripe_user which resides on the Stripe Database on Stripe servers.
            stripe_user=Customer.objects.get(user=request.user)

        except:

            # Creating a new user on Payments Database and also on Stripe Database on Stripe server
            stripe_user = Customer.create(request.user)

        # if stripe_user.stripe_customer.active_card: # checks if customer has card info on the Payments database
        #
        #     # Charging customer with the card saved on the database
        #     charge = stripe_user.charge(total_amount, send_receipt=settings.SEND_EMAIL_RECEIPTS)
        #
        #     new_food_order = FoodOrder(customer=request.user,
        #                                restaurant=rest,
        #                                order_price=total_amount,
        #                                charge_info=charge,
        #                                address_info=customerProf.address,
        #                                cart=food_cart.cart_serializable
        #                                )
        #     new_food_order.save()
        #
        #     context.update({'order_id': new_food_order.id})
        #     for item in food_cart.products:
        #         new_food_order.orderItems.add(item)
        #     food_cart.clear()
        #     request.session['CART-'+str(menu_id)] = food_cart.cart_serializable
        #     return render_to_response("meHungry_customer_website/order_receipt.html", context)

        #else:

        # Getting the credit card details submitted by the form
        token = request.POST['stripeToken']

        # Getting the users email address
        # user_email =  request.POST['stripeEmail']

        # Getting token information from the newly created token and updating the Customer table in Payments Database
        stripe_user.update_card(token)

        #Saving all updates
        stripe_user.save_card(stripe_user.stripe_customer)

        # Charging Customer for the

        charge = stripe_user.charge(total_amount, send_receipt=settings.SEND_EMAIL_RECEIPTS)
        new_food_order = Order(customer=request.user,
                               customer_phone=customerProf.phone,
                               restaurant=rest,
                               charge_info=charge.stripe_id,
                               order_price=total_amount,
                               address_info=customerProf.address,
                               order_for=order_day,
                               order_items=json_dump_cart)
        new_food_order.save()
        context.update({'order_id': new_food_order.id})
        food_cart.clear()
        request.session['CART-'+str(menu_id)] = food_cart.cart_serializable
        return render_to_response("meHungry_customer_website/order_receipt.html", context, context_instance=RequestContext(request))
    else:
        return redirect('homepage_view')


class SignupWizard(SessionWizardView):
    template_name = 'registration/signup_form.html'
    pin = Pin()

    def done(self, form_list, **kwargs):
        process_form_data(form_list, self)
        return redirect(user_home)

    def render_next_step(self, form, **kwargs):
        """
        In the validation step, ask the device to generate a challenge.
        """
        next_step = self.steps.next
        if next_step == '2':
            data = self.get_cleaned_data_for_step('1')
            phone = data['phone'].as_e164
            self.pin.generate_code()
            self.pin.send_pin(phone)
            self.request.session['signup_pin'] = self.pin.get_pin()
        return super(SignupWizard, self).render_next_step(form, **kwargs)

    def get_form_kwargs(self, step=None):
        if step == '2':
            return {'pin': self.request.session['signup_pin']}
        return {}


def process_form_data(form_list, wizard):
    cd = {'user_form': form_list[0].cleaned_data,
          'cust_form': form_list[1].cleaned_data}
    new_user = MyUser(first_name=cd['user_form']['first_name'],
                      last_name=cd['user_form']['last_name'],
                      email=cd['user_form']['email']
    )

    new_user.set_password(cd['user_form']["password"])
    new_user.save()
    group = Group.objects.get(name="Customer")
    group.user_set.add(new_user) # Setting newly registered user in the Customer Group

    new_user = authenticate(email=cd['user_form']['email'],
                            password=cd['user_form']["password"])
    login(wizard.request, new_user)
    stripe_user= Customer.create(new_user)

    CustomerProfile(user=new_user,
                    payment_info=stripe_user,
                    phone=cd['cust_form']['phone'],
                    address=cd['cust_form']['address']
    ).save()

@login_required
def my_orders(request):

    context = {}
    title = "meHungry My Orders"
    cust = request.user.id

    cust_orders = Order.objects.filter(customer_id=cust).order_by("-created")[:5]

    context.update({'title': title,
        'my_orders': cust_orders})

    return render_to_response('meHungry_customer_website/my_orders.html',context,context_instance=RequestContext(request))

# Here be DRF shit


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = (permissions.IsAdminUser,)

    queryset = MyUser.objects.all()
    serializer_class = UserSerializer

    @list_route()
    def recent_users(self, request):
        recent_users = MyUser.objects.all().order_by('-last_login')
        serializer = UserSerializer(recent_users, many=True, context={'request': request})
        return Response(serializer.data)


class AddressViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = (permissions.AllowAny,)

    queryset = Address.objects.all()
    serializer_class = AddressSerializer


class RestaurantViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = (permissions.AllowAny,)

    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializer


class FoodItemViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = (permissions.AllowAny,)

    queryset = FoodItem.objects.all()
    serializer_class = FoodItemSerializer


class MenuViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = (permissions.AllowAny,)

    queryset = Menu.objects.all()
    serializer_class = MenuSerializer


class WeeklyMenuViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = (permissions.AllowAny,)

    queryset = WeeklyMenu.objects.all()
    serializer_class = WeeklyMenuSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('address',)