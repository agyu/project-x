__author__ = 'pranaykumar'

from meHungry_customer_website.models import MyUser, CustomerProfile, Address
from django.forms import ModelForm
from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import ReadOnlyPasswordHashField, PasswordResetForm
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.formfields import PhoneNumberField
from phonenumber_field.widgets import PhoneNumberPrefixWidget
import logging
logger = logging.getLogger('logger')
logger.info('This is a simple log message')


class MetaForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(MetaForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'


class UserForm(MetaForm):
    class Meta:  # virtual_env/lib/python2.7/site-packages/django/forms/forms.py
        password = forms.PasswordInput(attrs={'required': 'true'})
        email = forms.EmailInput(attrs={'required': 'true'})
        model = MyUser
        fields = ['email', 'first_name', 'last_name', 'password']
        widgets = {
            'password': password,
            'email': email
        }


class PhoneForm(MetaForm):
    def __init__(self, *args, **kwargs):
        super(PhoneForm, self).__init__(*args, **kwargs)
        self.fields['address'].label = 'Preferred Delivery Location'

    class Meta:
        phone = PhoneNumberField()
        address = forms.ModelChoiceField( queryset=Address.objects.all())
        fields = ['phone', 'address']
        model = CustomerProfile
        widgets = {
            'phone': PhoneNumberPrefixWidget()
        }


class UpdateUserForm(MetaForm):
    class Meta:
        # forms.py
        model = MyUser
        email = forms.EmailInput()
        fields = ['email', 'first_name', 'last_name']
        widgets = {
            'email': email
        }


class AddressForm(MetaForm):
    def __init__(self, *args, **kwargs):
        super(AddressForm, self).__init__(*args, **kwargs)
        self.fields['address'].label = 'Preferred Delivery Location'
        self.fields['phone_notifications'].label = 'Text me notifications'
        self.fields['email_notifications'].label = 'Email me notifications'

    class Meta:
        # forms.py
        model = CustomerProfile
        address = forms.ModelChoiceField(label='Preferred Delivery Location',
                                         queryset=Address.objects.all())
        phone_notifications = forms.CheckboxInput()
        email_notifications = forms.CheckboxInput()

        fields = ['address', 'phone_notifications', 'email_notifications']


class PhoneVerifyForm(forms.Form):
    pin = forms.CharField(label='Verification Code', max_length=5, min_length=5)

    # error_messages = {
    #     'invalid_token': _('),
    #     }

    def __init__(self, pin, **args):
        super(PhoneVerifyForm, self).__init__(**args)
        self.gen_pin = pin

    def clean_pin(self):
        pin = self.cleaned_data.get('pin', '')
        if not self.gen_pin == int(pin):
            logger.info('!!!!!!!!!INVALID PIN SHOULD BE ' + str(self.gen_pin))
            raise forms.ValidationError('Invalid Pin')
        return pin


class EmailValidationForgotPW(PasswordResetForm):
    def __init__(self, *args, **kwargs):
        super(PasswordResetForm, self).__init__(*args, **kwargs)
        self.fields['email'].required = True
        self.fields['email'].widget = forms.EmailInput(attrs={'required': 'true'})

    def clean_email(self):
        email = self.cleaned_data['email']
        if not MyUser.objects.filter(email__iexact=email, is_active=True).exists():
            raise forms.ValidationError("There is no user registered with the specified email address.")
        return email


class MyUserCreationForm(forms.ModelForm):

    """ A form for creating new users.
    Includes all the required fields, plus a repeated password.
    """

    error_messages = {
        'duplicate_email': _("A user with that email already exists.")
    }

    password1 = forms.CharField(
        label=_("Password"),
        widget=forms.PasswordInput)

    class Meta:
        model = MyUser
        fields = ('email',)

    def clean_email(self):
        """ Clean form email.
        :return str email: cleaned email
        :raise forms.ValidationError: Email is duplicated
        """
        # Since EmailUser.email is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        email = self.cleaned_data["email"]
        try:
            get_user_model()._default_manager.get(email=email)
        except get_user_model().DoesNotExist:
            return email
        raise forms.ValidationError(
            self.error_messages['duplicate_email'],
            code='duplicate_email',
            )

    def save(self, commit=True):
        """ Save user.
        Save the provided password in hashed format.
        :return custom_user.models.EmailUser: user
        """
        user = super(MyUserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class MyUserChangeForm(forms.ModelForm):

    """ A form for updating users.
    Includes all the fields on the user, but replaces the password field
    with admin's password hash display field.
    """

    password = ReadOnlyPasswordHashField(label=_("Password"), help_text=_(
        "Raw passwords are not stored, so there is no way to see "
        "this user's password, but you can change the password "
        "using <a href=\"password/\">this form</a>."))

    class Meta:
        model = MyUser
        exclude = ()

    def __init__(self, *args, **kwargs):
        """ Init the form."""
        super(MyUserChangeForm, self).__init__(*args, **kwargs)
        f = self.fields.get('user_permissions', None)
        if f is not None:
            f.queryset = f.queryset.select_related('content_type')

    def clean_password(self):
        """ Clean password.
        Regardless of what the user provides, return the initial value.
        This is done here, rather than on the field, because the
        field does not have access to the initial value.
        :return str password:
        """
        return self.initial["password"]



